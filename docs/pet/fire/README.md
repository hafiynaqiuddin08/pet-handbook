---
forceTheme: red
---
# Fire Handbook
<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="../medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>

## Fire Team Rules

:::warning NOTE:
This is the handbook for the subsection of the Pinewood Emergency Team
**FIRE TEAM**

Please don't forget that is made for the fire team only, so no rules that are stated here apply on the other sub-divisions. 
Only if stated in their respective handbooks. 
:::

:::warning ORIGINAL POST:
You can also view basic information about the Fire Team here: [DevForum Post](http://bit.ly/38M8Bh3)
:::

### (FT. 1) Call on large fire
Fire Team are required to report any large fires using ``!call``.

### (FT. 2) On duty equipment
Fire Team are required to have a Fire Extinguisher and/or Fire Hose on them while On - Duty. They must also wear the fire suit.

### (FT. 3) Put out EVERY fire
Fire Team is required to put out any fires on sight, no matter what conditions.

### (FT. 4) Rescue anyone who needs it
Fire Team is also required to lead out visitors are any cost. Put their lives before yours.

## Points
The amount of points you can earn in a training/patrol is 3-5 points.

The point scale for latecomers is 1-2 points (for people who come 5 more minutes after the training begins)

## Rank system  
### Firefighter  
No requirements other than joining the group.  

### Trained Firefighter   
**Basic Firefighting Evaluation Required and...**   
Points: ``30`` 

Burning parts: ``5``  
Normal parts: ``20``  
Part speed: ``0``  
Spawn cooldown: ``0.2``  

### Experienced Firefighter  
**Intermediate Firefighting Required and...**  
Points: ``70``

Burning parts: ``7``  
Normal parts: ``30``  
Part speed: ``0``  
Spawn cooldown: ``0.2``  
 
### Elite Firefighter   
**Advanced Firefighting Evaluation Required and...**  
Points: ``130`` 

Burning parts: ``10``  
Normal parts: ``40``   
Part speed: ``0.5``  
Spawn cooldown: ``0.2``  

### Fire Marshall (Handpicked)
**Fire Marshall Evaluation**
1. Hosting a training (Overseen by a Deputy Chief or the Fire Chief)
2. Hosting a patrol (Optional, but recommended) (Also overseen by a Deputy Chief or the Fire Chief)

### Deputy Chief (Handpicked by Fire Chief)

:::tip
"Part speed" and “Spawn cooldown” are 2 settings that the evaluator will configure in the fire sim, don’t worry about those. Only focus on “Burning parts” and “Normal parts”. Firefighting evaluations created by HappyD631Sentinel, promotion system concept by Mister_Ultra
:::
## Self Patrol
### What is it?
This is a self-patrol system that has been established for people who can’t attend trainings, would like to earn points and rank up.

### How will it work?
Basically, to earn points in this system, you will need to record yourself patrolling the PBCC as a PET firefighter for a certain amount of time. You are to respond to emergencies and follow all PET handbook rules.

:::tip The amount of points you can earn whilst patrolling are as follow  
``15`` minutes = ``2`` points  
``30`` minutes = ``4`` points  
``45`` minutes = ``6`` points  
``60`` minutes = ``7`` points  
:::

Make sure to record evidence of your patrol! It must be included, or no points can be given out. Do not edit the video. The limit for how many recordings you post is 1 per week. Send your video (Recommended) (Uploaded to YouTube) to a Fire Marshall or above - message them on Roblox, or post your video on the group wall. An HR will soon respond and add the necessary amount of points.

### Earn points without a training
Record yourself putting out a massive fire completely.
Anyone who contributed to the process will earn 1 point, including yourself.
Remember, nothing can be given out if there is no evidence! When you’ve got the video and ready to post it, please send it to an HR (Message them on Roblox)
There is a limit of 1 recording per week if you are recording yourself putting out a massive fire.

:::danger PLEASE REMEMBER:
DON’T SEND VIDEOS ON THE WEEKENDS BECAUSE SELF-PATROL WILL BE CLOSED.
:::
Please remember to patrol PBCC on your own, without expecting any reward. After all, it’s our job to make the Pinewood Computer Core as safe as we can.

## PET Fire History
### Creation and chiefs
Group was officially created by Irreflexive (Uncondoned at that time) @ 1. 7. 2018. Now owned by Diddleshot.
:::tip Former Fire Chiefs
1. Lynbean
2. 190212
3. “Shadow” (unknown at the present day)
4. Hammy1237
5. Mister_Ultra
6. Odjuko
7. Ilikesinkingships
8. LENEMAR
9. RayzeYT
10. Hurrah123456
11. HappyD631Sentinel 
12. Wizertex
:::

### Fire HR's 
:::danger Fire Chief
 - [AnuCat](https://www.roblox.com/users/69576694/profile)
:::

:::danger Deputy Chief
 - [hbomb11111](https://www.roblox.com/users/92007486/profile)
 - [accidentalmeme](https://www.roblox.com/users/28135686/profile)
:::

:::danger Fire Marshall
 - [Chrissi006](https://www.roblox.com/users/447411004/profile)
 - [Ramzi65](https://www.roblox.com/users/100579562/profile)
 - [letsparty110](https://www.roblox.com/users/98832545/profile)
:::
## Feedback by our (former) members
:::tip - AnuCat (former Elite Firefighter, current Fire Chief)
“I enjoy being in the fire team. The friendly and consistent community and the fun trainings make it a great place to be.”
:::   

:::warning - Marikraften (Firefighter)
“I really enjoy being part of the Fire Team because you really get to feel like you’re part of a community, both of FT and PET, and the HRs are really chill/nice. It’s definitely a good experience.”
:::  

:::danger - ryuthelion (Firefighter)
" I just love the way we train with others. The trainers are well coordinated. I like how they are neat, and I still love it when they play around."   
:::  

:::tip - piemoo756 (Trained Firefighter)
" I love being in the fire team. Every day, I look forward to getting online. This is most likely because I love to help people. Fires put people’s lives at risk. If I get a chance to save someone’s life, then I’ll take it. This is probably also because my mother and father (before I was born) were caught in a house fire. They were saved by a few firefighters, and since then I’ve always looked up to them. Take that into example. Think about how you would feel and how you would make a child feel if you saved their parent’s lives. It gives the child gratefulness, that someone like that exists and is willing to help. It gives you a warm feeling in your heart, and reassurance that you are a good person.
:::  

:::warning - fhaidi (Former Director of Pinewood Builders Media and Firefighter)
Despite not being active in the division lately - though with deeper insight about it’s functions, I can safely say that: The fire team has to be - hands down - the best division Pinewood Emergency Team has to offer. From traditional, yet extensive, training sessions to a PLETHORA of fun activities - whether field trips all across ROBLOX, or even patrolling Pinewood facilities. The fire team has definitely widened the spectrum of activities throughout Pinewood Builders, it really strives to give PET a name limelight worthy. By far, I’ve been given a whole new experience with the Fire Team. My primary issue doesn’t lie with the fire team - however, PET as a whole. PET grows each day, and at its current state - it’s bound to have an activity center of its own! -
:::
